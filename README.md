# srrg_core_ros

This package contains some essential messages and nodes used in SRRG.
These include:
* logging in txt_io format
* broadcasting the transformations (offers a simplistic alternative to the ROS state publisher)
* srrg_map messages 
* services definitions

## Prerequisites

requires:
* [srrg_cmake_modules](https://gitlab.com/srrg-software/srrg_cmake_modules)
* [srrg_core](https://gitlab.com/srrg-software/srrg_core)

## Applications / Nodes

The following are some applications provided in the module
* `srrg_message_dumper_node`: listens ros msgs and serializes them in a txt file
* `srrg_state_publisher_node`: listens to an odometry topic and publishes the whole transform tree (replaces ros stata publisher)
* `rosbag_to_message_converter`: directly loads a ROS bag and converts selected messages to a BOSS/TXT message file

To get the help launch one of the previous programs with the `-h` option
Example:

    rosrun srrg_core srrg_messages_merger_app -h

For direct ROS bag to BOSS/TXT message file conversion use (stereo camera example):

    rosrun srrg_core_ros rosbag_to_message_converter -f stereo_trip.bag -camera-left-image /left/image_raw -camera-right-image /right/image_raw -camera-left-info /left/camera_info -camera-right-info /right/camera_info -stereo

If the camera info topics and/or the `-stereo` flag are omitted no post-processing is performed (and the images are stored as they appear in the ROS bag):

    rosrun srrg_core_ros rosbag_to_message_converter -f stereo_trip.bag -camera-left-image /left/image_raw -camera-right-image /right/image_raw

RGB-D bags can be processed as well (the Depth image is provided through the `-camera-right-image` flag, plus `-odometry` for odometry inclusion):

    rosrun srrg_core_ros rosbag_to_message_converter -f depth_trip.bag -camera-left-image /camera/rgb/image_raw -camera-right-image camera/depth/image_raw -camera-left-info /camera/rgb/camera_info -odometry /wheel_odometry 

### Dumping messages in txt_io format 

This can be done with the srrg_message_dumper_node. The dump will consist of a txt file, containing a line per message
and a directory containing binary data (e.g. images) in standard formats

The syntax is:
 `rosrun srrg_core_ros srrg_message_dumper_node [options]`
 where [options]: 
 * `-t`:  [string] ros topics of images
 * `-j`:  [string] ros topics of joints
 * `-laser`:  [string] ros topics of laser
 * `-base_link_frame_id <base frame id>` (default unset, if provided it will consider the odometry)
 * `-odom_frame_id <base frame id>` (default odom)
 * `-o`:       [string] output filename where to write the local maps. Default: out.txt
  
Example

    srrg_message_dumper_node -t /camera/depth/image_raw -t /camera/rgb/image_raw -o my_dump.txt


If the program works it will print a bunch of "**x**" (one per message) on the screen.
The camera info messages are automatically serialized and are assumed to have the name of the
same prefix of the image message.

Some basic interpolation is performed. If the robot has an odometry and
a transform tree, you can specify the base_link of the robot, and the messages 
will report the (interpolated) odometry.
Example

    srrg_message_dumper_node -t /camera/depth/image_raw -t /camera/rgb/image_raw -base_link_frame_id /base_link -o my_dump.txt


will dump the messages above, but each record will also report the odometry position estimated at the time
the data was taken. This simplifies using the data later on.
In this case you should see a bunch of "**o**" on the screen.

### Publishing the transform tree
To periodically publish the transform tree of the robot, in a synchronous
manner, you can use the `srrg_state_publisher_node`.
This module listens the odom topic and broadcasts the entire transform tree
of the robot each time an odometry is received. This is nice, since one does not
get the nasty sinchronization errors while playing a bag, since the tf tree is
synchronous with the odometry (and thus with the entire sensor stream).

Example
```
rosrun srrg_core_ros srrg_state_publisher_node my_transforms.txt
```
Where the file "my_transforms.txt" contains the transform tree in the following format
* each line contains an edge of the tree
* a line is made as follows
* STATIC_TRANSFORM from_frame  to_frame selector transform

Here:
* from_frame is the origin frame
* to_frame is the target frame
* selector is a string, specifying how the subsequent transform is expressed
 * Rt: the transforms are expressed as the 12 components of the R|t matrix, by rows
 * tq: are the 6 components x, y, z, qx, qy, qz of translation and normalized quaternion for rotation
 * trpy: are the 6 components x, y, z, roll, pitch, yaw

Example
```
STATIC_TRANSFORM /base_link camera_left_rgb tq -0.0388459 -0.0128734 0.5  -0.662503   0.440369  -0.338614   
STATIC_TRANSFORM camera_left_rgb camera_right_rgb tq -0.00483605   0.00249492    0.0268476 -0.000918612      0.34603     0.101196  
STATIC_TRANSFORM camera_left_rgb camera_front_rgb tq -0.0232331   0.113788  0.0740018   0.378772    0.18251 -0.0122294
STATIC_TRANSFORM /base_link laser_frame trpy 0 0 0.3 0 0 0 

```
Is a transform tree specifying a robot with three cameras and a laser.
The tree has 
* a link between the base and the left camera,
* a link between left and right camera
* a link between left and fromt camera
* the laser is on the axis of the wheels, 30 above base_link.
 
All transforms are specified in translation-quaternion form.


## Authors
* Giorgio Grisetti
* Jacopo Serafin
* Mayte Lazaro
* Maurilio Di Cicco
* Taigo Bonanni
* Bartolomeo Della Corte
* Dominik Schlegel

## License

BSD 2.0
