#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <srrg_messages/message_writer.h>
#include <srrg_messages/base_message.h>
#include <srrg_messages/joint_state_message.h>
#include <srrg_messages/pinhole_image_message.h>
#include <srrg_messages/laser_message.h>
#include <srrg_messages/imu_message.h>
#include <srrg_messages/spherical_image_message.h>
#include <srrg_messages/pose_message.h>

//ds ROS
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <ros/ros.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/MagneticField.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/LaserScan.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/TransformStamped.h>
#include <nav_msgs/Odometry.h>
#include <tf2_msgs/TFMessage.h>
#include <tf/transform_datatypes.h>

//ds opencv
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/version.hpp>
#include <opencv2/opencv.hpp>
#ifdef SRRG_CORE_HAS_OPENCV_CONTRIB
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/xfeatures2d/nonfree.hpp>
#endif
#if CV_MAJOR_VERSION == 3
#include <tf/transform_datatypes.h>
#endif

using namespace srrg_core;

//ds defines with desired floating point precision (e.g. double)
typedef double real;
typedef Eigen::Matrix<real, 3, 3> Matrix3;
typedef Eigen::Matrix<real, 6, 6> Matrix6;
typedef Eigen::Matrix<real, 3, 1> Vector3;
typedef cv::Vec<real, 5> Vector5;
typedef Eigen::Quaternion<real> Quaternion;
typedef Eigen::Matrix<real, 3, 4> ProjectionMatrix;
typedef Eigen::Transform<real, 3, Eigen::Isometry> TransformMatrix3D;

//ds parses the camera matrix specified by the topic from the provided bag
void configureMonocularCamera(const rosbag::Bag& bag_,
                              const std::string& topic_name_camera_info_,
                              Matrix3& camera_matrix_);

//ds computes the projection camera matrix, transforms and the undistortion and rectification maps for the stereo setup
void configureStereoCamera(const rosbag::Bag& bag_,
                           const std::string& topic_name_camera_info_left_,
                           const std::string& topic_name_camera_info_right_,
                           const std::string& topic_name_camera_left_to_imu_,
                           const std::string& topic_name_camera_right_to_imu_,
                           Matrix3& camera_matrix_,
                           TransformMatrix3D& camera_left_to_right_,
                           TransformMatrix3D& camera_left_to_imu_,
                           cv::Mat undistort_rectify_maps_left_[],
                           cv::Mat undistort_rectify_maps_right_[]);

//ml retrieves laser-base link which is assumed to be constant in the whole bag
void configureLaser(const rosbag::Bag& bag_,
		    const std::string& topic_name_laser_,
		    const std::string& base_link_frame_id_,
		    Eigen::Isometry3f& laser_offset);

Eigen::Isometry3f geomTransformToEigen(const geometry_msgs::Transform& tf_t) {
  Eigen::Isometry3f curr_transform = Eigen::Isometry3f::Identity();
  curr_transform.translation().x() = tf_t.translation.x;
  curr_transform.translation().y() = tf_t.translation.y;
  curr_transform.translation().z() = tf_t.translation.z;

  const Eigen::Quaternionf orientation_quaternion(tf_t.rotation.w,
                                                  tf_t.rotation.x,
                                                  tf_t.rotation.y,
                                                  tf_t.rotation.z);
  curr_transform.linear() = orientation_quaternion.toRotationMatrix();

  return curr_transform;
}

//ds entry point
int32_t main(int32_t argc_, char** argv_) {

  //ds arguments at least 2 - optional topic names
  if (argc_ < 3) {
    std::cerr << "ERROR: invalid call - use ./srrg_rosbag_to_message_converter_app -f <file_name_ros_bag>.bag \n" 
              << "-camera-left-image <topic> -camera-right-image <topic> \n" 
              << "-laser <topic>  (not working without odometry fixme) \n"
              << "-base-link-frame-id <frame_id> \n"
              <<   "[-camera-left-info <topic> -camera-right-info <topic> -stereo -odometry <topic>]" << std::endl;
    return EXIT_FAILURE;
  }

  //ds configuration
  std::string file_name_ros_bag              = "";
  std::string topic_name_camera_info_left    = "";
  std::string topic_name_camera_image_left   = "";
  std::string topic_name_camera_left_to_imu  = "";
  std::string topic_name_camera_info_right   = "";
  std::string topic_name_camera_image_right  = "";
  std::string topic_name_camera_right_to_imu = "";
  std::string topic_name_imu                 = "/imu";
  std::string topic_name_magnetic_field      = "";
  std::string topic_name_pressure            = "";
  std::string topic_name_odometry            = "";
  std::string topic_name_tf                  = "/tf_static";

  //ml laser configuration
  std::string topic_name_laser            = "";
  std::string base_link_frame_id          = "";
    
  bool option_have_camera_calibration = false;
  bool option_configure_stereo        = false;
  bool option_configure_laser         = false;
  
  //ds parse cmd arguments
  int32_t argc_parsed = 1;
  while(argc_parsed < argc_){
    if (!strcmp(argv_[argc_parsed],"-f")){
      argc_parsed++;
      file_name_ros_bag = argv_[argc_parsed];
    } else if (!strcmp(argv_[argc_parsed],"-camera-left-image")){
      argc_parsed++;
      topic_name_camera_image_left = argv_[argc_parsed];
    } else if (!strcmp(argv_[argc_parsed],"-camera-right-image")){
      argc_parsed++;
      topic_name_camera_image_right = argv_[argc_parsed];
    } else if (!strcmp(argv_[argc_parsed],"-camera-left-info")){
      argc_parsed++;
      topic_name_camera_info_left = argv_[argc_parsed];
    } else if (!strcmp(argv_[argc_parsed],"-camera-right-info")){
      argc_parsed++;
      topic_name_camera_info_right = argv_[argc_parsed];
    } else if (!strcmp(argv_[argc_parsed],"-odometry")){
      argc_parsed++;
      topic_name_odometry = argv_[argc_parsed];
    } else if (!strcmp(argv_[argc_parsed],"-stereo")){
      option_configure_stereo = true;
    } else if (!strcmp(argv_[argc_parsed],"-laser")){
      argc_parsed++;
      topic_name_laser = argv_[argc_parsed];
    } else if (!strcmp(argv_[argc_parsed],"-base-link-frame-id")){
      argc_parsed++;
      base_link_frame_id = argv_[argc_parsed];
    }
    
    argc_parsed++;
  }

  //ds minimal input validation
  if (file_name_ros_bag.length() == 0) {
    std::cerr << "ERROR: parameter -f <file_name_ros_bag>.bag not set" << std::endl;
    return EXIT_FAILURE;
  }
  if (!topic_name_camera_info_left.empty() && !topic_name_camera_info_right.empty()) {option_have_camera_calibration = true;}
  else if (topic_name_laser.empty()){
    option_configure_stereo = false;
    std::cerr << "ERROR: no camera calibration info provided - stereo configuration cancelled" << std::endl;
    return EXIT_FAILURE;
  }
  if (!topic_name_laser.empty() && !base_link_frame_id.empty()){
    option_configure_laser = true;
  } else if (!topic_name_laser.empty()) {
    option_configure_laser = false;
    std::cerr << "ERROR: no base link info provided - laser configuration cancelled" << std::endl;
    return EXIT_FAILURE;
  }

  //ds attempt to open the ROS bag - may throw BagException
  rosbag::Bag bag(file_name_ros_bag);
  const std::string file_name_ros_bag_clean = file_name_ros_bag.substr(0, file_name_ros_bag.length()-4);

  //ds outfile configuration
  const std::string filename_messages(file_name_ros_bag_clean+".txt");

  //ds log configuration
  std::cerr << "              input ROS bag: " << file_name_ros_bag << std::endl;
  if (!topic_name_camera_info_left.empty())    {std::cerr << "topic name camera info left: " << topic_name_camera_info_left << std::endl;}
  if (!topic_name_camera_image_left.empty())   {std::cerr << "                camera left: " << topic_name_camera_image_left << std::endl;}
  if (!topic_name_camera_left_to_imu.empty())  {std::cerr << "    pose camera left to imu: " << topic_name_camera_left_to_imu << std::endl;}
  if (!topic_name_camera_info_right.empty())   {std::cerr << "          camera info right: " << topic_name_camera_info_right << std::endl;}
  if (!topic_name_camera_image_right.empty())  {std::cerr << "               camera right: " << topic_name_camera_image_right << std::endl;}
  if (!topic_name_camera_right_to_imu.empty()) {std::cerr << "   pose camera right to imu: " << topic_name_camera_right_to_imu << std::endl;}
  if (!topic_name_imu.empty())                 {std::cerr << "                        imu: " << topic_name_imu << std::endl;}
  if (!topic_name_magnetic_field.empty())      {std::cerr << "             magnetic field: " << topic_name_magnetic_field << std::endl;}
  if (!topic_name_pressure.empty())            {std::cerr << "                   pressure: " << topic_name_pressure << std::endl;}
  if (!topic_name_odometry.empty())            {std::cerr << "        topic name odometry: " << topic_name_odometry << std::endl;}
  if (!topic_name_laser.empty())               {std::cerr << "           topic name laser: " << topic_name_laser << std::endl;}
  if (!base_link_frame_id.empty())             {std::cerr << "         base link frame id: " << base_link_frame_id << std::endl;}

  std::cerr << "  output messages file name: " << filename_messages << std::endl;
  std::cerr << "        stereo camera setup: " << option_configure_stereo << std::endl;

  //ds camera configuration
  Matrix3 camera_matrix_left(Matrix3::Identity());
  Matrix3 camera_matrix_right(Matrix3::Identity());
  TransformMatrix3D camera_left_to_right(TransformMatrix3D::Identity());
  TransformMatrix3D camera_left_to_imu(TransformMatrix3D::Identity());

  //ds undistortion/rectification (only used when option_configure_stereo == true)
  cv::Mat undistort_rectify_maps_left[2];
  cv::Mat undistort_rectify_maps_right[2];

  //ds if we have a stereo camera setup
  if (option_configure_stereo) {

    //ds configure stereo setup
    configureStereoCamera(bag,
                          topic_name_camera_info_left,
                          topic_name_camera_info_right,
                          topic_name_camera_left_to_imu,
                          topic_name_camera_right_to_imu,
                          camera_matrix_left,
                          camera_left_to_right,
                          camera_left_to_imu,
                          undistort_rectify_maps_left,
                          undistort_rectify_maps_right);

    //ds identical for the undistorted and rectified setup
    camera_matrix_right = camera_matrix_left;
  } else {

    //ds if we have camera information
    if (!topic_name_camera_info_left.empty()) {
      configureMonocularCamera(bag, topic_name_camera_info_left, camera_matrix_left);
    }

    //ds if we have camera information
    if (!topic_name_camera_info_right.empty()) {
      configureMonocularCamera(bag, topic_name_camera_info_right, camera_matrix_right);
    }
  }

  //ml laser configuration
  Eigen::Isometry3f laser_offset(Eigen::Isometry3f::Identity());
  Eigen::Isometry3f camera_left_offset;
  camera_left_offset.setIdentity();

  srrg_core::PoseMessage last_odom_msg;
  last_odom_msg.setPose(Eigen::Isometry3d::Identity());
  srrg_core::PoseMessage last_laser_odom_msg;
  last_laser_odom_msg.setPose(Eigen::Isometry3d::Identity());
  
  //ml if we have a laser setup
  if (option_configure_laser) {
    configureLaser(bag,
		   topic_name_laser,
		   base_link_frame_id,
		   laser_offset);
    std::cerr << "GOT Laser Offset: " << std::endl << laser_offset.matrix() << std::endl;
  }
  
  //ds obtain a bag viewer
  rosbag::View bag_viewer(bag);

  //ds open a message writer in the current directory
  MessageWriter message_writer;
  message_writer.open(filename_messages);

  //ds info
  uint64_t number_of_messages = 0;


  std::string last_frame = "camera_color_optical_frame";

  //ds loop over the bag - writing to disk
  for (rosbag::View::iterator itMessage = bag_viewer.begin(); itMessage != bag_viewer.end(); ++itMessage) {

    const ros::Time timestamp = itMessage->getTime();
    std::string message_topic = itMessage->getTopic();

    if (message_topic == topic_name_tf) {
      tf2_msgs::TFMessageConstPtr ros_message = itMessage->instantiate<tf2_msgs::TFMessage>();


      auto transforms = ros_message->transforms;

      int i = 0;
      while (i < ros_message->transforms.size()) {
        if (last_frame == base_link_frame_id) {
          Eigen::Isometry3f curr_transform = geomTransformToEigen(transforms[i].transform);
          camera_left_offset = curr_transform * camera_left_offset;
          break;
        }
        if (transforms[i].child_frame_id == last_frame) {
          const geometry_msgs::Transform& geom_t = transforms[i].transform;
          Eigen::Isometry3f curr_transform = geomTransformToEigen(transforms[i].transform);
          last_frame = transforms[i].header.frame_id;
          camera_left_offset = curr_transform * camera_left_offset;
          i = 0;
          continue;
        }
        ++i;
      }
    }

    //ds match message to topic - UGLY UGLY TODO collapse this whole switch bomb
    if (message_topic == topic_name_camera_image_left || message_topic == topic_name_camera_image_right) {
      sensor_msgs::ImageConstPtr ros_message  = itMessage->instantiate<sensor_msgs::Image>();
      srrg_core::PinholeImageMessage* message = new srrg_core::PinholeImageMessage();
      message->setSeq(ros_message->header.seq);
      message->setTimestamp(ros_message->header.stamp.toSec());


      //ds consistent left/right camera naming
      if (message_topic == topic_name_camera_image_left) {
        message->setFrameId("camera_left");
        message->setTopic("/camera_left/image_raw");
      } else {
        message->setFrameId("camera_right");
        message->setTopic("/camera_right/image_raw");
      }

      message->setOdometry(last_odom_msg.pose().cast<float>());
      //ds copy image data
      cv_bridge::CvImagePtr image_handler;
      image_handler = cv_bridge::toCvCopy(ros_message, ros_message->encoding);
      cv::Mat image(image_handler->image);

      if(strcmp(ros_message->encoding.c_str(), "bayer_rggb8") == 0) {
        cv::Mat image_col(ros_message->height, ros_message->width, CV_8UC3);
        cv::cvtColor(image, image_col, CV_BayerBG2BGR);
        image=cv::Mat(ros_message->height, ros_message->width, CV_8UC3);
        cv::cvtColor(image_col, image, CV_BGR2RGB);
        std::cerr << "B";
      }
      
      //ds if left image
      if (message_topic == topic_name_camera_image_left) {

        //ds set projection matrix offset if available
        message->setOffset(camera_left_to_imu.cast<float>());

        //ds if we have calibration information
        if (option_have_camera_calibration) {
          message->setCameraMatrix(camera_matrix_left.cast<float>());

          //ds if we have a stereo camera setup
          if (option_configure_stereo) {

            //ds rectify image
            cv::remap(image, image, undistort_rectify_maps_left[0], undistort_rectify_maps_left[1], cv::INTER_LINEAR);
          }
        }
        std::cerr << "L";

      //ds right image (can also be DEPTH)
      } else {

        //ds if we have calibration information
        if (option_have_camera_calibration) {
          message->setCameraMatrix(camera_matrix_right.cast<float>());

          //ds if we have a stereo camera setup
          if (option_configure_stereo) {

            //ds set projection matrix offset
            message->setOffset(camera_left_to_right.cast<float>());
            
            //ds rectify image
            cv::remap(image, image, undistort_rectify_maps_right[0], undistort_rectify_maps_right[1], cv::INTER_LINEAR);
          }
        }
        std::cerr << "R";
      }

      //ds set image to message
      message->setImage(image);
      //      message->setOffset(camera_left_offset);
      //ds save message to disk
      message_writer.writeMessage(*message);
      delete message;
    } else if (message_topic == topic_name_imu) {
      sensor_msgs::ImuConstPtr ros_message = itMessage->instantiate<sensor_msgs::Imu>();
      srrg_core::CIMUMessage* message      = new srrg_core::CIMUMessage();
      message->setSeq(ros_message->header.seq);
      message->setTimestamp(ros_message->header.stamp.toSec());
      message->setFrameId(ros_message->header.frame_id);
      message->setTopic(message_topic);

      //ds checked
      Eigen::Vector3d angular_velocity(ros_message->angular_velocity.x, ros_message->angular_velocity.y, ros_message->angular_velocity.z);
      message->setAngularVelocity(angular_velocity);
      Eigen::Vector3d linear_acceleration(ros_message->linear_acceleration.x, ros_message->linear_acceleration.y, ros_message->linear_acceleration.z);
      message->setLinearAcceleration(linear_acceleration);
      Eigen::Quaterniond orientation(ros_message->orientation.w, ros_message->orientation.x, ros_message->orientation.y, ros_message->orientation.z);
      message->setOrientation(orientation);

      //ds save message to disk
      message_writer.writeMessage(*message);
      delete message;
      std::cerr << "I";

    } else if (message_topic == topic_name_magnetic_field) {
      //ds not implemented yet
    } else if (message_topic == topic_name_pressure) {
      //ds not implemented yet
    } else if (message_topic == topic_name_odometry) {
      nav_msgs::OdometryConstPtr ros_message = itMessage->instantiate<nav_msgs::Odometry>();
      srrg_core::PoseMessage* message        = new srrg_core::PoseMessage();
      message->setSeq(ros_message->header.seq);
      message->setTimestamp(ros_message->header.stamp.toSec());
      message->setFrameId(ros_message->header.frame_id);
      message->setTopic(message_topic);

      //ds compute isometry: translation
      TransformMatrix3D pose(TransformMatrix3D::Identity());
      pose.translation().x() = ros_message->pose.pose.position.x;
      pose.translation().y() = ros_message->pose.pose.position.y;
      pose.translation().z() = ros_message->pose.pose.position.z;

      //ds compute isometry: orientation
      const Eigen::Quaterniond orientation_quaternion(ros_message->pose.pose.orientation.w,
                                                      ros_message->pose.pose.orientation.x,
                                                      ros_message->pose.pose.orientation.y,
                                                      ros_message->pose.pose.orientation.z);
      pose.linear() = orientation_quaternion.toRotationMatrix();

      //ds convert covariance to eigen matrix
      Matrix6 covariance(Matrix6::Identity());
      for (uint32_t row = 0; row < 6; ++row) {
        for (uint32_t col = 0; col < 6; ++col) {
          uint32_t sequential_index = row*6+col;
          covariance(row, col) = ros_message->pose.covariance.elems[sequential_index];
        }
      }

      //ds set message
      message->setPose(static_cast<Eigen::Isometry3d>(pose));
      message->setCovariance(static_cast<Matrix6d>(covariance));

      //ml save message for future use in laser dump
      last_odom_msg = *message;
      
      //ds save message to disk
      //ml commented since PoseMessage::toStream() not supported
//      message_writer.writeMessage(*message);
      delete message;
      std::cerr << "O";
    } else if (message_topic == topic_name_laser) {
      sensor_msgs::LaserScanConstPtr ros_message = itMessage->instantiate<sensor_msgs::LaserScan>();
      srrg_core::LaserMessage* message           = new srrg_core::LaserMessage();
      message->setSeq(ros_message->header.seq);
      message->setTimestamp(ros_message->header.stamp.toSec());
      message->setFrameId(ros_message->header.frame_id);
      message->setTopic(message_topic);
      message->setOffset(laser_offset);

      //ml fill in stuff
      message->setMinAngle(ros_message->angle_min);
      message->setMaxAngle(ros_message->angle_max);
      message->setAngleIncrement(ros_message->angle_increment);
      message->setMinRange(ros_message->range_min);
      message->setMaxRange(ros_message->range_max);
      message->setTimeIncrement(ros_message->time_increment);
      message->setScanTime(ros_message->scan_time);
      
      std::vector<float> ranges;
      ranges.resize(ros_message->ranges.size());
      for (size_t i = 0; i < ros_message->ranges.size(); i++){
	if ((ros_message->ranges[i] < ros_message->range_min) || std::isnan(ros_message->ranges[i]))
	  ranges[i]=0;
	else if (ros_message->ranges[i] > ros_message->range_max)
	  ranges[i]=ros_message->range_max;
	else
	  ranges[i]=ros_message->ranges[i];
      }
      message->setRanges(ranges);

      std::vector<float> intensities;
      intensities.resize(ros_message->intensities.size());
      for (size_t i = 0; i < ros_message->intensities.size(); i++)
	intensities[i]=ros_message->intensities[i];
      message->setIntensities(intensities);

      //ml check movement since last laser message
      Eigen::Isometry3d movement = last_laser_odom_msg.pose().inverse()*last_odom_msg.pose();
      Eigen::Rotation2Dd rotation;
      rotation.matrix() = movement.linear().block<2,2>(0,0);
      if (movement.translation().norm() < 1e-3 && fabs(rotation.angle()) < 1e-4){
	delete message;
	continue;
      } else {
	message->setOdometry(last_odom_msg.pose().cast<float>());
	last_laser_odom_msg = last_odom_msg;

	//ml save message to disk
	message_writer.writeMessage(*message);
	delete message;
	std::cerr << "L";
      }
      
    } 
    ++number_of_messages;
  }
  std::cerr << std::endl;
  std::cerr << "converted messages: " << number_of_messages << std::endl;
  bag.close();
  return 0;
}

void configureMonocularCamera(const rosbag::Bag& bag_,
                              const std::string& topic_name_camera_info_,
                              Matrix3& camera_matrix_) {

  //ds open the bag
  rosbag::View bag_viewer(bag_);

  //ds loop over the bag looking for the components to buffer
  for (rosbag::View::iterator itMessage = bag_viewer.begin(); itMessage != bag_viewer.end(); ++itMessage) {
    const std::string message_topic = itMessage->getTopic();

    //ds match message to topic
    if (message_topic == topic_name_camera_info_) {
      sensor_msgs::CameraInfo::ConstPtr ros_message = itMessage->instantiate<sensor_msgs::CameraInfo>();

      //ds set camera matrix
      for(uint32_t row = 0; row < 3; ++row) {
        for(uint32_t col = 0; col < 3; ++col) {
          camera_matrix_(row,col) = ros_message->K[3*row+col];
        }
      }

      //ds done
      std::cerr << "loaded camera matrix: " << topic_name_camera_info_ << std::endl;
      std::cerr << camera_matrix_ << std::endl;
      break;
    }
  }
}

void configureStereoCamera(const rosbag::Bag& bag_,
                           const std::string& topic_name_camera_info_left_,
                           const std::string& topic_name_camera_info_right_,
                           const std::string& topic_name_camera_left_to_imu_,
                           const std::string& topic_name_camera_right_to_imu_,
                           Matrix3& camera_matrix_,
                           TransformMatrix3D& camera_left_to_right_,
                           TransformMatrix3D& camera_left_to_imu_,
                           cv::Mat undistort_rectify_maps_left_[],
                           cv::Mat undistort_rectify_maps_right_[]) {

  //ds open the bag
  rosbag::View bag_viewer(bag_);

  //ds buffered components - set once and assumed to be constant over the whole dataset
  cv::Mat_<real> camera_matrix_left(3, 3);   camera_matrix_left = 0;
  cv::Mat_<real> camera_matrix_right(3, 3); camera_matrix_right = 0;
  TransformMatrix3D camera_left_to_imu(TransformMatrix3D::Identity());
  TransformMatrix3D camera_right_to_imu(TransformMatrix3D::Identity());
  cv::Mat_<real> projection_matrix_left(3, 4);  projection_matrix_left  = 0;
  cv::Mat_<real> projection_matrix_right(3, 4); projection_matrix_right = 0;
  Vector5 distortion_coefficients_left;   distortion_coefficients_left = 0;
  Vector5 distortion_coefficients_right; distortion_coefficients_right = 0;
  cv::Mat_<real> rectification_matrix_left(3, 3);  rectification_matrix_left  = 0;
  cv::Mat_<real> rectification_matrix_right(3, 3); rectification_matrix_right = 0;
  uint32_t image_width_pixels  = 0;
  uint32_t image_height_pixels = 0;

  //ds loop over the bag looking for the components to buffer
  for (rosbag::View::iterator itMessage = bag_viewer.begin(); itMessage != bag_viewer.end(); ++itMessage) {

    const ros::Time timestamp = itMessage->getTime();
    std::string message_topic = itMessage->getTopic();

    //ds match message to topic - UGLY UGLY TODO collapse this whole switch bomb
    if (message_topic == topic_name_camera_info_left_) {
      sensor_msgs::CameraInfo::ConstPtr ros_message = itMessage->instantiate<sensor_msgs::CameraInfo>();
      image_width_pixels  = ros_message->width;
      image_height_pixels = ros_message->height;

      //ds set camera matrix
      for(uint32_t row = 0; row < 3; ++row) {
        for(uint32_t col = 0; col < 3; ++col) {
          camera_matrix_left(row,col) = ros_message->K[3*row+col];
        }
      }

      //ds set projection matrix
      for(uint32_t row = 0; row < 3; ++row) {
        for(uint32_t col = 0; col < 4; ++col) {
          projection_matrix_left(row,col) = ros_message->P[4*row+col];
        }
      }

      //ds set distortion coefficients
      for(uint32_t u = 0; u < 5; ++u) {
        distortion_coefficients_left[u] = ros_message->D[u];
      }

      //ds set rectification matrix
      for(uint32_t row = 0; row < 3; ++row) {
        for(uint32_t col = 0; col < 3; ++col) {
          rectification_matrix_left(row,col) = ros_message->R[3*row+col];
        }
      }
    } else if (message_topic == topic_name_camera_left_to_imu_) {
      geometry_msgs::Pose::ConstPtr ros_message = itMessage->instantiate<geometry_msgs::Pose>();

      //ds buffer transform components
      Vector3 translation(ros_message->position.x, ros_message->position.y, ros_message->position.z);
      Quaternion rotation(ros_message->orientation.w, ros_message->orientation.x, ros_message->orientation.y, ros_message->orientation.z);

      //ds set full transform
      camera_left_to_imu.translation() = translation;
      camera_left_to_imu.linear()      = rotation.toRotationMatrix();
    } else if (message_topic == topic_name_camera_info_right_) {
      sensor_msgs::CameraInfo::ConstPtr ros_message = itMessage->instantiate<sensor_msgs::CameraInfo>();
      image_width_pixels  = ros_message->width;
      image_height_pixels = ros_message->height;

      //ds set camera matrix
      for(uint32_t row = 0; row < 3; ++row) {
        for(uint32_t col = 0; col < 3; ++col) {
          camera_matrix_right(row,col) = ros_message->K[3*row+col];
        }
      }

      //ds set projection matrix
      for(uint32_t row = 0; row < 3; ++row) {
        for(uint32_t col = 0; col < 4; ++col) {
          projection_matrix_right(row,col) = ros_message->P[4*row+col];
        }
      }

      //ds set distortion coefficients
      for(uint32_t u = 0; u < 5; ++u) {
        distortion_coefficients_right[u] = ros_message->D[u];
      }

      //ds set rectification matrix
      for(uint32_t row = 0; row < 3; ++row) {
        for(uint32_t col = 0; col < 3; ++col) {
          rectification_matrix_right(row,col) = ros_message->R[3*row+col];
        }
      }
    } else if (message_topic == topic_name_camera_right_to_imu_) {
      geometry_msgs::Pose::ConstPtr ros_message = itMessage->instantiate<geometry_msgs::Pose>();

      //ds buffer transform components
      Vector3 translation(ros_message->position.x, ros_message->position.y, ros_message->position.z);
      Quaternion rotation(ros_message->orientation.w, ros_message->orientation.x, ros_message->orientation.y, ros_message->orientation.z);

      //ds set full transform
      camera_right_to_imu.translation() = translation;
      camera_right_to_imu.linear()      = rotation.toRotationMatrix();
    }

    //ds as soon as we have all the essential information required
    if (cv::norm(camera_matrix_left)  != 0 &&
        cv::norm(camera_matrix_right) != 0 ) {
      break;
    }
  }

  //ds check if a value has not been set
  if (cv::norm(camera_matrix_left) == 0) {
    std::cerr << "ERROR: camera matrix left not set" << std::endl;
    exit(EXIT_FAILURE);
  }
  if (cv::norm(camera_matrix_right) == 0) {
    std::cerr << "ERROR: camera matrix right not set" << std::endl;
    exit(EXIT_FAILURE);
  }
  if (camera_left_to_imu.translation().norm() == 0) {
    std::cerr << "WARNING: transform camera left to IMU not set - Press [ENTER] to continue or [CTRL]+[C] to abort" << std::endl;
    Vector3 translation(0., -0.02, 0.);
    Quaternion rotation(0.5, 0.5, -0.5, 0.5);
    camera_left_to_imu.translation() = translation;
    camera_left_to_imu.linear()      = rotation.toRotationMatrix();
    getchar();
  }
  if (camera_right_to_imu.translation().norm() == 0) {
    std::cerr << "WARNING: transform camera right to IMU not set - Press [ENTER] to continue or [CTRL]+[C] to abort" << std::endl;
    Vector3 translation(-0.21, -0.02, 0.);
    Quaternion rotation(0.5, 0.5, -0.5, 0.5);
    camera_right_to_imu.translation() = translation;
    camera_right_to_imu.linear()      = rotation.toRotationMatrix();
    getchar();
  }

  //ds compute undistorted and rectified mappings
  cv::initUndistortRectifyMap(camera_matrix_left,
                              distortion_coefficients_left,
                              rectification_matrix_left,
                              projection_matrix_left,
                              cv::Size(image_width_pixels, image_height_pixels),
                              CV_16SC2,
                              undistort_rectify_maps_left_[0],
                              undistort_rectify_maps_left_[1]);
  cv::initUndistortRectifyMap(camera_matrix_right,
                              distortion_coefficients_right,
                              rectification_matrix_right,
                              projection_matrix_right,
                              cv::Size(image_width_pixels, image_height_pixels),
                              CV_16SC2,
                              undistort_rectify_maps_right_[0],
                              undistort_rectify_maps_right_[1]);

  //ds check if rectification failed
  if (cv::norm(undistort_rectify_maps_left_[0]) == 0 || cv::norm(undistort_rectify_maps_left_[1]) == 0) {
    std::cerr << "ERROR: unable to undistort and rectify camera left" << std::endl;
    exit(EXIT_FAILURE);
  }
  if (cv::norm(undistort_rectify_maps_right_[0]) == 0 || cv::norm(undistort_rectify_maps_right_[1]) == 0) {
    std::cerr << "ERROR: unable to undistort and rectify camera right" << std::endl;
    exit(EXIT_FAILURE);
  }

  //ds info
  std::cerr << "undistorting and rectifying input: " << std::endl;
  std::cerr << "projection matrix LEFT: " << topic_name_camera_info_left_ << std::endl;
  std::cerr << projection_matrix_left << std::endl;
  std::cerr << "projection matrix RIGHT: " << topic_name_camera_info_right_ << std::endl;
  std::cerr << projection_matrix_right << std::endl;

  //ds get camera matrices to eigen space for convenience
  camera_matrix_ = Matrix3::Zero();
  Vector3 offset(Vector3::Zero());
  camera_left_to_right_ = TransformMatrix3D::Identity();

  //ds use rectification result
  for(uint32_t row = 0; row < 3; ++row) {
    for(uint32_t col = 0; col < 3; ++col) {
      camera_matrix_(row,col) = projection_matrix_left(row,col);
    }
  }

  //ds get right projection matrix to eigen space
  ProjectionMatrix projection_matrix_right_eigen(ProjectionMatrix::Zero());
  for(uint32_t row = 0; row < 3; ++row) {
    for(uint32_t col = 0; col < 4; ++col) {
      projection_matrix_right_eigen(row,col) = projection_matrix_right(row,col);
    }
  }

  //ds compute offset for right camera in order to reconstruct projection matrix form txt_io message
  offset = camera_matrix_.fullPivLu().solve(projection_matrix_right_eigen.block<3,1>(0,3));
  camera_left_to_right_.translation() = -offset;
}



void configureLaser(const rosbag::Bag& bag_,
		    const std::string& topic_name_laser_,
		    const std::string& base_link_frame_id_,
		    Eigen::Isometry3f& laser_offset_){

  //ml open the bag
  rosbag::View bag_viewer(bag_);

  std::string laser_frame;
  bool got_laser = false;
  bool got_tf = false;
  //ml iterate until laser_offset retrieved
  for (rosbag::View::iterator itMessage = bag_viewer.begin(); itMessage != bag_viewer.end(); ++itMessage) {
    if (got_tf)
      break;
    
    const std::string message_topic = itMessage->getTopic();

    //ml looking for a laser message
    if (!got_laser && message_topic == topic_name_laser_) {
      sensor_msgs::LaserScanConstPtr ros_message = itMessage->instantiate<sensor_msgs::LaserScan>();
      laser_frame = ros_message->header.frame_id;
      got_laser = true;
      std::cerr << "GOT Laser Frame: " << laser_frame << std::endl;
    }

    //ml looking for tf from laser_frame to base_link
    if (got_laser && message_topic == "/tf"){
      std::cerr << "Looking for transform from: " << base_link_frame_id_ << " to: " << laser_frame << std::endl;
      tf2_msgs::TFMessageConstPtr ros_message = itMessage->instantiate<tf2_msgs::TFMessage>();

      for (geometry_msgs::TransformStamped tfmsg: ros_message->transforms){

	//ml removing first "/" present
	std::string tfmsg_frame_id, tfmsg_child_frame_id;
	if (tfmsg.header.frame_id.length() && tfmsg.header.frame_id[0]=='/')
	  tfmsg_frame_id = tfmsg.header.frame_id.substr(1,tfmsg.header.frame_id.length());
	else
	  tfmsg_frame_id = tfmsg.header.frame_id;

	if (tfmsg.child_frame_id.length() && tfmsg.child_frame_id[0]=='/')
	  tfmsg_child_frame_id = tfmsg.child_frame_id.substr(1,tfmsg.child_frame_id.length());
	else
	  tfmsg_child_frame_id = tfmsg.child_frame_id;

	if (tfmsg_frame_id == base_link_frame_id_ && tfmsg_child_frame_id == laser_frame){
	  Eigen::Quaternionf q;
	  q.x() = tfmsg.transform.rotation.x;
	  q.y() = tfmsg.transform.rotation.y;
	  q.z() = tfmsg.transform.rotation.z;
	  q.w() = tfmsg.transform.rotation.w;
	  laser_offset_.linear()=q.toRotationMatrix();
	  laser_offset_.translation()=Eigen::Vector3f(tfmsg.transform.translation.x,
						      tfmsg.transform.translation.y,
						      tfmsg.transform.translation.z);
	  
	  std::cerr << "GOT Base-Laser transform" << std::endl;
	  got_tf = true;
	  break;
	}
      }

    }
  }
}
