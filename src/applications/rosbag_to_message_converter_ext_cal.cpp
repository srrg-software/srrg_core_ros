#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <srrg_messages/message_writer.h>
#include <srrg_messages/base_message.h>
#include <srrg_messages/joint_state_message.h>
#include <srrg_messages/pinhole_image_message.h>
#include <srrg_messages/laser_message.h>
#include <srrg_messages/imu_message.h>
#include <srrg_messages/spherical_image_message.h>
#include <srrg_messages/pose_message.h>

//ds ROS
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <ros/ros.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/MagneticField.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Pose.h>
#include <nav_msgs/Odometry.h>
#include "yaml-cpp/yaml.h"

//ds opencv
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/version.hpp>
#include <opencv2/opencv.hpp>
#ifdef SRRG_CORE_HAS_OPENCV_CONTRIB
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/xfeatures2d/nonfree.hpp>
#endif
#if CV_MAJOR_VERSION == 3
#include <tf/transform_datatypes.h>
#endif

using namespace srrg_core;

//ds defines with desired floating point precision (e.g. double)
typedef double real;
typedef Eigen::Matrix<real, 3, 3> Matrix3;
typedef Eigen::Matrix<real, 6, 6> Matrix6;
typedef Eigen::Matrix<real, 3, 1> Vector3;
typedef cv::Vec<real, 5> Vector5;
typedef Eigen::Quaternion<real> Quaternion;
typedef Eigen::Matrix<real, 3, 4> ProjectionMatrix;
typedef Eigen::Transform<real, 3, Eigen::Isometry> TransformMatrix3D;

bool loadYAMLCalib(std::string fname, double K[], double R[], double P[], double D[], uint32_t *width, uint32_t *height);

//ds parses the camera matrix specified by the topic from the provided bag
void configureMonocularCamera(const rosbag::Bag& bag_,
                              const std::string& topic_name_camera_info_,
                              Matrix3& camera_matrix_);

//ds computes the projection camera matrix, transforms and the undistortion and rectification maps for the stereo setup
void configureStereoCamera(const rosbag::Bag& bag_,
                           const std::string& topic_name_camera_info_left_,
                           const std::string& topic_name_camera_info_right_,
                           const std::string& topic_name_camera_left_to_imu_,
                           const std::string& topic_name_camera_right_to_imu_,
                           Matrix3& camera_matrix_,
                           TransformMatrix3D& camera_left_to_right_,
                           TransformMatrix3D& camera_left_to_imu_,
                           cv::Mat undistort_rectify_maps_left_[],
                           cv::Mat undistort_rectify_maps_right_[]);

//ds entry point
int32_t main(int32_t argc_, char** argv_) {

  //ds arguments at least 2 - optional topic names
  if (argc_ < 3) {
    std::cerr << "ERROR: invalid call - use ./srrg_rosbag_to_message_converter_app -f <file_name_ros_bag>.bag "
                 "-camera-left-image <topic> -camera-right-image <topic> "
                 "[-camera-left-info <topic> -camera-right-info <topic> -stereo -odometry <topic>]" << std::endl;
    return EXIT_FAILURE;
  }

  //ds configuration
  std::string file_name_ros_bag              = "";
  std::string yaml_left_cam_info    = "";
  std::string topic_name_camera_image_left   = "";
  std::string topic_name_camera_left_to_imu  = "";
  std::string yaml_right_cam_info   = "";
  std::string topic_name_camera_image_right  = "";
  std::string topic_name_camera_right_to_imu = "";
  std::string topic_name_imu                 = "/imu";
  std::string topic_name_magnetic_field      = "";
  std::string topic_name_pressure            = "";
  std::string topic_name_odometry            = "";

  bool option_have_camera_calibration = false;
  bool option_configure_stereo        = false;

  //ds parse cmd arguments
  int32_t argc_parsed = 1;
  while(argc_parsed < argc_){
    if (!strcmp(argv_[argc_parsed],"-f")){
      argc_parsed++;
      file_name_ros_bag = argv_[argc_parsed];
    } else if (!strcmp(argv_[argc_parsed],"-camera-left-image")){
      argc_parsed++;
      topic_name_camera_image_left = argv_[argc_parsed];
    } else if (!strcmp(argv_[argc_parsed],"-camera-right-image")){
      argc_parsed++;
      topic_name_camera_image_right = argv_[argc_parsed];
    } else if (!strcmp(argv_[argc_parsed],"-camera-left-info")){
      argc_parsed++;
      yaml_left_cam_info = argv_[argc_parsed];
    } else if (!strcmp(argv_[argc_parsed],"-camera-right-info")){
      argc_parsed++;
      yaml_right_cam_info = argv_[argc_parsed];
    } else if (!strcmp(argv_[argc_parsed],"-odometry")){
      argc_parsed++;
      topic_name_odometry = argv_[argc_parsed];
    } else if (!strcmp(argv_[argc_parsed],"-stereo")){
      option_configure_stereo = true;
    }
    argc_parsed++;
  }



  //ds minimal input validation
  if (file_name_ros_bag.length() == 0) {
    std::cerr << "ERROR: parameter -f <file_name_ros_bag>.bag not set" << std::endl;
    return EXIT_FAILURE;
  }
  if (!yaml_left_cam_info.empty() && !yaml_right_cam_info.empty()) {option_have_camera_calibration = true;}
  else {
    option_configure_stereo = false;
    std::cerr << "ERROR: no camera calibration info provided - stereo configuration cancelled" << std::endl;
    return EXIT_FAILURE;
  }

  //ds attempt to open the ROS bag - may throw BagException
  rosbag::Bag bag(file_name_ros_bag);
  const std::string file_name_ros_bag_clean = file_name_ros_bag.substr(0, file_name_ros_bag.length()-4);

  //ds outfile configuration
  const std::string filename_messages(file_name_ros_bag_clean+".txt");

  //ds log configuration
  std::cerr << "              input ROS bag: " << file_name_ros_bag << std::endl;
  if (!yaml_left_cam_info.empty())    {std::cerr << "topic name camera info left: " << yaml_left_cam_info << std::endl;}
  if (!topic_name_camera_image_left.empty())   {std::cerr << "                camera left: " << topic_name_camera_image_left << std::endl;}
  if (!topic_name_camera_left_to_imu.empty())  {std::cerr << "    pose camera left to imu: " << topic_name_camera_left_to_imu << std::endl;}
  if (!yaml_right_cam_info.empty())   {std::cerr << "          camera info right: " << yaml_right_cam_info << std::endl;}
  if (!topic_name_camera_image_right.empty())  {std::cerr << "               camera right: " << topic_name_camera_image_right << std::endl;}
  if (!topic_name_camera_right_to_imu.empty()) {std::cerr << "   pose camera right to imu: " << topic_name_camera_right_to_imu << std::endl;}
  if (!topic_name_imu.empty())                 {std::cerr << "                        imu: " << topic_name_imu << std::endl;}
  if (!topic_name_magnetic_field.empty())      {std::cerr << "             magnetic field: " << topic_name_magnetic_field << std::endl;}
  if (!topic_name_pressure.empty())            {std::cerr << "                   pressure: " << topic_name_pressure << std::endl;}
  if (!topic_name_odometry.empty())            {std::cerr << "        topic name odometry: " << topic_name_odometry << std::endl;}
  std::cerr << "  output messages file name: " << filename_messages << std::endl;
  std::cerr << "        stereo camera setup: " << option_configure_stereo << std::endl;

  //ds camera configuration
  Matrix3 camera_matrix_left(Matrix3::Identity());
  Matrix3 camera_matrix_right(Matrix3::Identity());
  TransformMatrix3D camera_left_to_right(TransformMatrix3D::Identity());
  TransformMatrix3D camera_left_to_imu(TransformMatrix3D::Identity());

  //ds undistortion/rectification (only used when option_configure_stereo == true)
  cv::Mat undistort_rectify_maps_left[2];
  cv::Mat undistort_rectify_maps_right[2];

  //ds if we have a stereo camera setup
  if (option_configure_stereo) {

    //ds configure stereo setup
    configureStereoCamera(bag,
                          yaml_left_cam_info,
                          yaml_right_cam_info,
                          topic_name_camera_left_to_imu,
                          topic_name_camera_right_to_imu,
                          camera_matrix_left,
                          camera_left_to_right,
                          camera_left_to_imu,
                          undistort_rectify_maps_left,
                          undistort_rectify_maps_right);

    //ds identical for the undistorted and rectified setup
    camera_matrix_right = camera_matrix_left;
  } else {

    //ds if we have camera information
    if (!yaml_left_cam_info.empty()) {
      configureMonocularCamera(bag, yaml_left_cam_info, camera_matrix_left);
    }

    //ds if we have camera information
    if (!yaml_right_cam_info.empty()) {
      configureMonocularCamera(bag, yaml_right_cam_info, camera_matrix_right);
    }
  }

  //ds obtain a bag viewer
  rosbag::View bag_viewer(bag);

  //ds open a message writer in the current directory
  MessageWriter message_writer;
  message_writer.open(filename_messages);

  //ds info
  uint64_t number_of_messages = 0;

  //ds loop over the bag - writing to disk
  for (rosbag::View::iterator itMessage = bag_viewer.begin(); itMessage != bag_viewer.end(); ++itMessage) {

    const ros::Time timestamp = itMessage->getTime();
    std::string message_topic = itMessage->getTopic();

    //ds match message to topic - UGLY UGLY TODO collapse this whole switch bomb
    if (message_topic == topic_name_camera_image_left || message_topic == topic_name_camera_image_right) {
      sensor_msgs::ImageConstPtr ros_message  = itMessage->instantiate<sensor_msgs::Image>();
      srrg_core::PinholeImageMessage* message = new srrg_core::PinholeImageMessage();
      message->setSeq(ros_message->header.seq);
      message->setTimestamp(ros_message->header.stamp.toSec());

      //ds consistent left/right camera naming
      if (message_topic == topic_name_camera_image_left) {
        message->setFrameId("camera_left");
        message->setTopic("/camera_left/image_raw");
      } else {
        message->setFrameId("camera_right");
        message->setTopic("/camera_right/image_raw");
      }

      //ds copy image data
      cv_bridge::CvImagePtr image_handler;
      image_handler = cv_bridge::toCvCopy(ros_message, ros_message->encoding);
      cv::Mat image(image_handler->image);

      if(strcmp(ros_message->encoding.c_str(), "bayer_rggb8") == 0) {
        cv::Mat image_col(ros_message->height, ros_message->width, CV_8UC3);
        cv::cvtColor(image, image_col, CV_BayerBG2BGR);
        image=cv::Mat(ros_message->height, ros_message->width, CV_8UC3);
        cv::cvtColor(image_col, image, CV_BGR2RGB);
        std::cerr << "B";
      }
      
      //ds if left image
      if (message_topic == topic_name_camera_image_left) {

        //ds set projection matrix offset if available
        message->setOffset(camera_left_to_imu.cast<float>());

        //ds if we have calibration information
        if (option_have_camera_calibration) {
          message->setCameraMatrix(camera_matrix_left.cast<float>());

          //ds if we have a stereo camera setup
          if (option_configure_stereo) {

            //ds rectify image
            cv::remap(image, image, undistort_rectify_maps_left[0], undistort_rectify_maps_left[1], cv::INTER_LINEAR);
          }
        }
        std::cerr << "L";

      //ds right image (can also be DEPTH)
      } else {

        //ds if we have calibration information
        if (option_have_camera_calibration) {
          message->setCameraMatrix(camera_matrix_right.cast<float>());

          //ds if we have a stereo camera setup
          if (option_configure_stereo) {

            //ds set projection matrix offset
            message->setOffset(camera_left_to_right.cast<float>());

            //ds rectify image
            cv::remap(image, image, undistort_rectify_maps_right[0], undistort_rectify_maps_right[1], cv::INTER_LINEAR);
          }
        }
        std::cerr << "R";
      }

      //ds set image to message
      message->setImage(image);

      //ds save message to disk
      message_writer.writeMessage(*message);
      delete message;
    } else if (message_topic == topic_name_imu) {
      sensor_msgs::ImuConstPtr ros_message = itMessage->instantiate<sensor_msgs::Imu>();
      srrg_core::CIMUMessage* message      = new srrg_core::CIMUMessage();
      message->setSeq(ros_message->header.seq);
      message->setTimestamp(ros_message->header.stamp.toSec());
      message->setFrameId(ros_message->header.frame_id);
      message->setTopic(message_topic);

      //ds checked
      Eigen::Vector3d angular_velocity(ros_message->angular_velocity.x, ros_message->angular_velocity.y, ros_message->angular_velocity.z);
      message->setAngularVelocity(angular_velocity);
      Eigen::Vector3d linear_acceleration(ros_message->linear_acceleration.x, ros_message->linear_acceleration.y, ros_message->linear_acceleration.z);
      message->setLinearAcceleration(linear_acceleration);
      Eigen::Quaterniond orientation(ros_message->orientation.w, ros_message->orientation.x, ros_message->orientation.y, ros_message->orientation.z);
      message->setOrientation(orientation);

      //ds save message to disk
      message_writer.writeMessage(*message);
      delete message;
      std::cerr << "I";

    } else if (message_topic == topic_name_magnetic_field) {
      //ds not implemented yet
    } else if (message_topic == topic_name_pressure) {
      //ds not implemented yet
    } else if (message_topic == topic_name_odometry) {
      nav_msgs::OdometryConstPtr ros_message = itMessage->instantiate<nav_msgs::Odometry>();
      srrg_core::PoseMessage* message        = new srrg_core::PoseMessage();
      message->setSeq(ros_message->header.seq);
      message->setTimestamp(ros_message->header.stamp.toSec());
      message->setFrameId(ros_message->header.frame_id);
      message->setTopic(message_topic);

      //ds compute isometry: translation
      TransformMatrix3D pose(TransformMatrix3D::Identity());
      pose.translation().x() = ros_message->pose.pose.position.x;
      pose.translation().y() = ros_message->pose.pose.position.y;
      pose.translation().z() = ros_message->pose.pose.position.z;

      //ds compute isometry: orientation
      const Eigen::Quaterniond orientation_quaternion(ros_message->pose.pose.orientation.w,
                                                      ros_message->pose.pose.orientation.x,
                                                      ros_message->pose.pose.orientation.y,
                                                      ros_message->pose.pose.orientation.z);
      pose.linear() = orientation_quaternion.toRotationMatrix();

      //ds convert covariance to eigen matrix
      Matrix6 covariance(Matrix6::Identity());
      for (uint32_t row = 0; row < 6; ++row) {
        for (uint32_t col = 0; col < 6; ++col) {
          uint32_t sequential_index = row*6+col;
          covariance(row, col) = ros_message->pose.covariance.elems[sequential_index];
        }
      }

      //ds set message
      message->setPose(static_cast<Eigen::Isometry3d>(pose));
      message->setCovariance(static_cast<Matrix6d>(covariance));

      //ds save message to disk
      message_writer.writeMessage(*message);
      delete message;
      std::cerr << "O";
    }
    ++number_of_messages;
  }
  std::cerr << std::endl;
  std::cerr << "converted messages: " << number_of_messages << std::endl;
  bag.close();
  return 0;
}

void configureMonocularCamera(const rosbag::Bag& bag_,
                              const std::string& topic_name_camera_info_,
                              Matrix3& camera_matrix_) {

  //ds open the bag
  rosbag::View bag_viewer(bag_);

  //ds loop over the bag looking for the components to buffer
  for (rosbag::View::iterator itMessage = bag_viewer.begin(); itMessage != bag_viewer.end(); ++itMessage) {
    const std::string message_topic = itMessage->getTopic();

    //ds match message to topic
    if (message_topic == topic_name_camera_info_) {
      sensor_msgs::CameraInfo::ConstPtr ros_message = itMessage->instantiate<sensor_msgs::CameraInfo>();

      //ds set camera matrix
      for(uint32_t row = 0; row < 3; ++row) {
        for(uint32_t col = 0; col < 3; ++col) {
          camera_matrix_(row,col) = ros_message->K[3*row+col];
        }
      }

      //ds done
      std::cerr << "loaded camera matrix: " << topic_name_camera_info_ << std::endl;
      std::cerr << camera_matrix_ << std::endl;
      break;
    }
  }
}

void configureStereoCamera(const rosbag::Bag& bag_,
                           const std::string& topic_name_camera_info_left_,
                           const std::string& topic_name_camera_info_right_,
                           const std::string& topic_name_camera_left_to_imu_,
                           const std::string& topic_name_camera_right_to_imu_,
                           Matrix3& camera_matrix_,
                           TransformMatrix3D& camera_left_to_right_,
                           TransformMatrix3D& camera_left_to_imu_,
                           cv::Mat undistort_rectify_maps_left_[],
                           cv::Mat undistort_rectify_maps_right_[]) {

  //ds open the bag
  rosbag::View bag_viewer(bag_);

  //ds buffered components - set once and assumed to be constant over the whole dataset
  cv::Mat_<real> camera_matrix_left(3, 3);   camera_matrix_left = 0;
  cv::Mat_<real> camera_matrix_right(3, 3); camera_matrix_right = 0;
  TransformMatrix3D camera_left_to_imu(TransformMatrix3D::Identity());
  TransformMatrix3D camera_right_to_imu(TransformMatrix3D::Identity());
  cv::Mat_<real> projection_matrix_left(3, 4);  projection_matrix_left  = 0;
  cv::Mat_<real> projection_matrix_right(3, 4); projection_matrix_right = 0;
  Vector5 distortion_coefficients_left;   distortion_coefficients_left = 0;
  Vector5 distortion_coefficients_right; distortion_coefficients_right = 0;
  cv::Mat_<real> rectification_matrix_left(3, 3);  rectification_matrix_left  = 0;
  cv::Mat_<real> rectification_matrix_right(3, 3); rectification_matrix_right = 0;
  uint32_t image_width_pixels  = 0;
  uint32_t image_height_pixels = 0;
  //if (message_topic == topic_name_camera_info_left_) {
  double K[9], R[9], P[12], D[5];
      loadYAMLCalib(topic_name_camera_info_left_, K, R, P, D, &image_width_pixels, &image_height_pixels);

      //ds set camera matrix
      for(uint32_t row = 0; row < 3; ++row) {
        for(uint32_t col = 0; col < 3; ++col) {
          camera_matrix_left(row,col) = K[3*row+col];
        }
      }

      //ds set projection matrix
      for(uint32_t row = 0; row < 3; ++row) {
        for(uint32_t col = 0; col < 4; ++col) {
          projection_matrix_left(row,col) = P[4*row+col];
        }
      }

      //ds set distortion coefficients
      for(uint32_t u = 0; u < 5; ++u) {
        distortion_coefficients_left[u] = D[u];
      }

      //ds set rectification matrix
      for(uint32_t row = 0; row < 3; ++row) {
        for(uint32_t col = 0; col < 3; ++col) {
          rectification_matrix_left(row,col) = R[3*row+col];
        }
      }
    //} else if (message_topic == topic_name_camera_info_right_) {
      loadYAMLCalib(topic_name_camera_info_right_, K, R, P, D, &image_width_pixels, &image_height_pixels);

      //ds set camera matrix
      for(uint32_t row = 0; row < 3; ++row) {
        for(uint32_t col = 0; col < 3; ++col) {
          camera_matrix_right(row,col) = K[3*row+col];
        }
      }

      //ds set projection matrix
      for(uint32_t row = 0; row < 3; ++row) {
        for(uint32_t col = 0; col < 4; ++col) {
          projection_matrix_right(row,col) = P[4*row+col];
        }
      }

      //ds set distortion coefficients
      for(uint32_t u = 0; u < 5; ++u) {
        distortion_coefficients_right[u] = D[u];
      }

      //ds set rectification matrix
      for(uint32_t row = 0; row < 3; ++row) {
        for(uint32_t col = 0; col < 3; ++col) {
          rectification_matrix_right(row,col) = R[3*row+col];
        }
      }
    //}

  //ds loop over the bag looking for the components to buffer
  for (rosbag::View::iterator itMessage = bag_viewer.begin(); itMessage != bag_viewer.end(); ++itMessage) {

    const ros::Time timestamp = itMessage->getTime();
    std::string message_topic = itMessage->getTopic();

    //ds match message to topic - UGLY UGLY TODO collapse this whole switch bomb
    if (message_topic == topic_name_camera_left_to_imu_) {
      geometry_msgs::Pose::ConstPtr ros_message = itMessage->instantiate<geometry_msgs::Pose>();

      //ds buffer transform components
      Vector3 translation(ros_message->position.x, ros_message->position.y, ros_message->position.z);
      Quaternion rotation(ros_message->orientation.w, ros_message->orientation.x, ros_message->orientation.y, ros_message->orientation.z);

      //ds set full transform
      camera_left_to_imu.translation() = translation;
      camera_left_to_imu.linear()      = rotation.toRotationMatrix();
    } else if (message_topic == topic_name_camera_right_to_imu_) {
      geometry_msgs::Pose::ConstPtr ros_message = itMessage->instantiate<geometry_msgs::Pose>();

      //ds buffer transform components
      Vector3 translation(ros_message->position.x, ros_message->position.y, ros_message->position.z);
      Quaternion rotation(ros_message->orientation.w, ros_message->orientation.x, ros_message->orientation.y, ros_message->orientation.z);

      //ds set full transform
      camera_right_to_imu.translation() = translation;
      camera_right_to_imu.linear()      = rotation.toRotationMatrix();
    }

    //ds as soon as we have all the essential information required
    if (cv::norm(camera_matrix_left)  != 0 &&
        cv::norm(camera_matrix_right) != 0 ) {
      break;
    }
  }

  //ds check if a value has not been set
  if (cv::norm(camera_matrix_left) == 0) {
    std::cerr << "ERROR: camera matrix left not set" << std::endl;
    exit(EXIT_FAILURE);
  }
  if (cv::norm(camera_matrix_right) == 0) {
    std::cerr << "ERROR: camera matrix right not set" << std::endl;
    exit(EXIT_FAILURE);
  }
  if (camera_left_to_imu.translation().norm() == 0) {
    std::cerr << "WARNING: transform camera left to IMU not set - Press [ENTER] to continue or [CTRL]+[C] to abort" << std::endl;
    Vector3 translation(0., -0.02, 0.);
    Quaternion rotation(0.5, 0.5, -0.5, 0.5);
    camera_left_to_imu.translation() = translation;
    camera_left_to_imu.linear()      = rotation.toRotationMatrix();
    getchar();
  }
  if (camera_right_to_imu.translation().norm() == 0) {
    std::cerr << "WARNING: transform camera right to IMU not set - Press [ENTER] to continue or [CTRL]+[C] to abort" << std::endl;
    Vector3 translation(-0.21, -0.02, 0.);
    Quaternion rotation(0.5, 0.5, -0.5, 0.5);
    camera_right_to_imu.translation() = translation;
    camera_right_to_imu.linear()      = rotation.toRotationMatrix();
    getchar();
  }

  //ds compute undistorted and rectified mappings
  cv::initUndistortRectifyMap(camera_matrix_left,
                              distortion_coefficients_left,
                              rectification_matrix_left,
                              projection_matrix_left,
                              cv::Size(image_width_pixels, image_height_pixels),
                              CV_16SC2,
                              undistort_rectify_maps_left_[0],
                              undistort_rectify_maps_left_[1]);
  cv::initUndistortRectifyMap(camera_matrix_right,
                              distortion_coefficients_right,
                              rectification_matrix_right,
                              projection_matrix_right,
                              cv::Size(image_width_pixels, image_height_pixels),
                              CV_16SC2,
                              undistort_rectify_maps_right_[0],
                              undistort_rectify_maps_right_[1]);

  //ds check if rectification failed
  if (cv::norm(undistort_rectify_maps_left_[0]) == 0 || cv::norm(undistort_rectify_maps_left_[1]) == 0) {
    std::cerr << "ERROR: unable to undistort and rectify camera left" << std::endl;
    exit(EXIT_FAILURE);
  }
  if (cv::norm(undistort_rectify_maps_right_[0]) == 0 || cv::norm(undistort_rectify_maps_right_[1]) == 0) {
    std::cerr << "ERROR: unable to undistort and rectify camera right" << std::endl;
    exit(EXIT_FAILURE);
  }

  //ds info
  std::cerr << "undistorting and rectifying input: " << std::endl;
  std::cerr << "projection matrix LEFT: " << topic_name_camera_info_left_ << std::endl;
  std::cerr << projection_matrix_left << std::endl;
  std::cerr << "projection matrix RIGHT: " << topic_name_camera_info_right_ << std::endl;
  std::cerr << projection_matrix_right << std::endl;

  //ds get camera matrices to eigen space for convenience
  camera_matrix_ = Matrix3::Zero();
  Vector3 offset(Vector3::Zero());
  camera_left_to_right_ = TransformMatrix3D::Identity();

  //ds use rectification result
  for(uint32_t row = 0; row < 3; ++row) {
    for(uint32_t col = 0; col < 3; ++col) {
      camera_matrix_(row,col) = projection_matrix_left(row,col);
    }
  }

  //ds get right projection matrix to eigen space
  ProjectionMatrix projection_matrix_right_eigen(ProjectionMatrix::Zero());
  for(uint32_t row = 0; row < 3; ++row) {
    for(uint32_t col = 0; col < 4; ++col) {
      projection_matrix_right_eigen(row,col) = projection_matrix_right(row,col);
    }
  }

  //ds compute offset for right camera in order to reconstruct projection matrix form txt_io message
  offset = camera_matrix_.fullPivLu().solve(projection_matrix_right_eigen.block<3,1>(0,3));
  camera_left_to_right_.translation() = -offset;
}

bool loadYAMLCalib(std::string fname, double K[], double R[], double P[], double D[], uint32_t *width, uint32_t *height) {
  std::vector<double> K_(9);
  std::vector<double> R_(9);
  std::vector<double> P_(12);
  std::vector<double> D_(5);
  YAML::Node calib=YAML::LoadFile(fname);
  *width=calib["image_width"].as<uint32_t>();
  *height=calib["image_height"].as<uint32_t>();
  D_=(calib["distortion_coefficients"])["data"].as<std::vector<double>>();
  K_=(calib["camera_matrix"])["data"].as<std::vector<double>>();
  R_=(calib["rectification_matrix"])["data"].as<std::vector<double>>();
  P_=(calib["projection_matrix"])["data"].as<std::vector<double>>();
  memcpy(K, &K_[0], 9*sizeof(double));
  memcpy(R, &R_[0], 9*sizeof(double));
  memcpy(P, &P_[0], 12*sizeof(double));
  memcpy(D, &D_[0], 5*sizeof(double));
  return true;
}
